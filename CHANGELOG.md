## v2.0.1

- Verified on DevEco Studio: NEXT Beta1-5.0.3.806, SDK:API12 Release(5.0.0.66)

## v2.0.0

- DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)
- ArkTs语法适配

## v1.0.4

- 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）

## v1.0.3

- 修改类名，赋予一般意义

## v1.0.2

1. api8升级成api9
2. 添加扩展接口

## v1.0.0

- 已实现功能
  1. 单任务下载；
  2. 多任务串、并行下载；
  3. 超多任务串、并行下载；
  4. 设置任务优先级；
  5. 提供不同场景的任务监听；
- 不支持
  1. 断点下载；
  2. 分块下载；
  3. 自定义notification功能ohos暂时不支持，属于系统能力
