# okdownload

## Overview

okdownload implements download engine that is reliable, flexible, high-performing, and powerful. It supports a single download task, a batch of serial/parallel download tasks, and listening for the download task status.

## Display Effects

![Image text](screenshot/okdownload.gif)

## How to Install

```shell
ohpm install @ohos/okdownload
```

For details, see [Installing an OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use


1. Create a single download task.

```
import {DownloadTask} from '@ohos/okdownload'

this.task = new DownloadTask.Builder(url, filename)
    .build()
this.task.execute(listener) // Execute tasks synchronously.
```

2. Create multiple download tasks.

 ```
import {DownloadContextListener} from '@ohos/okdownload'
import {DownloadContext, QueueSet} from '@ohos/okdownload'
import {DownloadTask} from '@ohos/okdownload'

let queueSet = new QueueSet();
queueSet.setParentPath('queue')
let builder = queueSet.commit()

let url = "http://cdn.llsapp.com/yy/image/3b0430db-5ff4-455c-9c8d-0213eea7b6c4.jpg"
let boundTask = builder.bindUrl(url, 'task1.jpg')

url = "http://cdn.llsapp.com/yy/image/3b0430db-5ff4-455c-9c8d-0213eea7b6c4.jpg"
boundTask = builder.bindUrl(url, 'task2.jpg')

url = "https://dldir1.qq.com/qqfile/QQforMac/QQ_V6.2.0.dmg"
boundTask = builder.bindUrl(url, 'QQ_V6.2.0.dmg')

this.downloadContext = builder.build();
this.downloadContext.start(listener, false) // true: synchronous download; false: asynchronous download
 ```

## Available APIs
`let builder = new DownloadTask.Builder(url, filename)`
1. Sets the priority.
   `builder.setPriority(priority : number)`
2. Sets the file name.
   `builder.setFilename(filename: string)`
3. Sets the request header.
   `builder.setHeaderMapFields(headerMapFields: object)`
4. Creates a download task.
   `builder.build()`
5. Executes a download task.
   `task.execute(listener: DownloadListener)`
6. Executes a download task asynchronously.
   `task.enqueue(listener: DownloadListener)`
7. Executes a download task asynchronously.
   `static enqueueTasks(tasks: DownloadTask[], listener: DownloadListener)`
8. `DownloadContext`
   Executes download tasks in serial mode.
   `startOnSerial(listener: DownloadListener)`
9. Executes download tasks in parallel mode.
   `startOnParallel(listener: DownloadListener)`
10. Starts a download task.
    `start(listener: DownloadListener, isSerial: boolean)`

## Constraints

okdownload has been verified in the following versions:

- DevEco Studio: 4.1 Canary (4.1.3.317), OpenHarmony SDK: API11 (4.1.0.36)

## Directory Structure
````
|---- okdownload
|   |---- entry  # Sample code
|       |----src
|            |----customRadio
|            |----model
|            |----util
|   |---- okdownload  # okdownload library
|       |---- src
|           |---- main
|               |---- ets
|                   |---- common
|                       |---- http # Network request
|                       |---- okdownload
|                           |---- breakpoint # Breakpoint information
|                           |---- cause # Download status
|                           |---- connection # Network interfaces
|                           |---- dispatcher # Download task dispatching
|                           |---- download # Download service logic
|                           |---- exception # Exception
|                           |---- interceptor # Interceptor
|                           |---- listener # Task listening
|                           |---- DownloadContext.ets # Serial/parallel download tasks (set by QueueSet)
|                           |---- DownloadListener.ets # Download status callback
|                           |---- DownloadMonitor.ets # Download task monitoring
|                           |---- DownloadTask.ets # Single download task
|                           |---- OkDownload.ets # Entry class, responsible for download task dispatching
|                           |---- SpeedCalculator.ets # Download speed calculation
|                           |---- StatusUtil.ets # Download status checking
|                       |---- sqlite # Database
|   |---- README_EN.md  # Readme
````
## How to Contribute
If you find any problem when using okdownload, submit an [Issue](https://gitee.com/openharmony-sig/okdownload/issues) or a [PR](https://gitee.com/openharmony-sig/okdownload/pulls).

## License

The ohos_coap library is based on [Apache License 2.0](https://gitee.com/openharmony-sig/okdownload/blob/master/LICENSE).
