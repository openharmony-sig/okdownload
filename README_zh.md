# okdownload

## 简介

>可靠，灵活，高性能以及强大的下载引擎。支持单任务下载，多任务串、并行下载，设置多种任务监听等。

## 效果展示：

![Image text](screenshot/okdownload.gif)

## 下载安装

```shell
ohpm install @ohos/okdownload
```

OpenHarmony ohpm环境配置等更多内容，请参考 [如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md) 。

## 使用说明


1. 创建单个下载任务：

```
import {DownloadTask} from '@ohos/okdownload'

this.task = new DownloadTask.Builder(url, filename)
    .build()
this.task.execute(listener) //同步执行任务
```

2. 创建多个下载任务：

 ```
import {DownloadContextListener} from '@ohos/okdownload'
import {DownloadContext, QueueSet} from '@ohos/okdownload'
import {DownloadTask} from '@ohos/okdownload'

let queueSet = new QueueSet();
queueSet.setParentPath('queue')
let builder = queueSet.commit()

let url = "http://cdn.llsapp.com/yy/image/3b0430db-5ff4-455c-9c8d-0213eea7b6c4.jpg"
let boundTask = builder.bindUrl(url, 'task1.jpg')

url = "http://cdn.llsapp.com/yy/image/3b0430db-5ff4-455c-9c8d-0213eea7b6c4.jpg"
boundTask = builder.bindUrl(url, 'task2.jpg')

url = "https://dldir1.qq.com/qqfile/QQforMac/QQ_V6.2.0.dmg"
boundTask = builder.bindUrl(url, 'QQ_V6.2.0.dmg')

this.downloadContext = builder.build();
this.downloadContext.start(listener, false) //true为同步下载， false为异步下载
 ```

## 接口说明
`let builder = new DownloadTask.Builder(url, filename)`
1. 设置优先级
   `builder.setPriority(priority : number)`
2. 设置文件名
   `builder.setFilename(filename: string)`
3. 设置请求头
   `builder.setHeaderMapFields(headerMapFields: object)`
4. 创建DownloadTask
   `builder.build()`
5. 执行下载任务
   `task.execute(listener: DownloadListener)`
6. 异步执行下载任务
   `task.enqueue(listener: DownloadListener)`
7. 异步执行下载任务
   `static enqueueTasks(tasks: DownloadTask[], listener: DownloadListener)`
8. `DownloadContext`
   同步执行任务
   `startOnSerial(listener: DownloadListener)`
9. 异步执行任务
   `startOnParallel(listener: DownloadListener)`
10. 开启下载任务
    `start(listener: DownloadListener, isSerial: boolean)`

## 约束与限制

在下述版本验证通过：

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK:API12 Release(5.0.0.66)
- DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)。

## 目录结构
````
|---- okdownload
|   |---- entry  # 示例代码文件夹
|       |----src
|            |----customRadio
|            |----model
|            |----util
|   |---- okdownload  # 库文件夹
|       |---- src
|           |---- main
|               |---- ets
|                   |---- common
|                       |---- http # 网络请求
|                       |---- okdownload
|                           |---- breakpoint # 断点信息
|                           |---- cause # 下载状态
|                           |---- connection # 网络接口
|                           |---- dispatcher # 下载任务装配
|                           |---- download # 下载逻辑
|                           |---- exception # 异常
|                           |---- interceptor #拦截器
|                           |---- listener #任务监听
|                           |---- DownloadContext.ets # 多个下载任务串/并行下载，使用QueueSet来做设置
|                           |---- DownloadListener.ets #下载状态回调接口定义
|                           |---- DownloadMonitor.ets #下载任务监控
|                           |---- DownloadTask.ets # 单个下载任务
|                           |---- OkDownload.ets # 入口类，负责下载任务装配
|                           |---- SpeedCalculator.ets # 下载速度计算
|                           |---- StatusUtil.ets # 检查下载文件是否已经下载完成等
|                       |---- sqlite #数据库
|   |---- README.md  # 安装使用方法
|   |---- README_zh.md  # 安装使用方法
````
## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/okdownload/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/okdownload/pulls) 。

## 版权和许可信息

本项目基于 [Apache License 2.0](https://gitee.com/openharmony-sig/okdownload/blob/master/LICENSE) ，请自由地享受和参与开源。