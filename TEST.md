# text-encoding单元测试用例

该测试用例基于OpenHarmony系统下，进行单元测试

单元测试用例覆盖情况

|                         接口名                          |是否通过	|备注|
|:----------------------------------------------------:|:---:|:---:|
|          Builder(url, parentPath, filename)          |    pass        |       |
|    setFilenameFromResponse(filenameFromResponse)     |pass   |        |
|                setPriority(priority)                 |   pass  |          |
|                setFilename(filename)                 | pass  |          |
|                    getFilename()                     | pass  |          |
|  setPassIfAlreadyCompleted(passIfAlreadyCompleted)   | pass  |          |
|            setWifiRequired(wifiRequired)             |  pass |          |
|               isFilenameFromResponse()               |  pass |          |
|                 setHeaderMapFields()                 |pass  |     |
|                 getHeaderMapFields()                 | pass  |          |
|                       getId()                        | pass  |          |
|              isPassIfAlreadyCompleted()              |  pass |       |
|                   isWifiRequired()                   | pass  |       |
|                  enqueue (listener)                  |pass   |          |
|                      cancel ()                       | pass  |      |
|                     isStarted()                      |  pass |       |
|                    execute(task)                     |  pass |       |
|                       stop ()                        |  pass |       |
|                      cancel(id)                      |  pass |       |
|                  findSameTask(task)                  |  pass |       |
|                   isRunning (task)                   |  pass |       |
|                   isPending (task)                   |  pass |       |
|                       getUrl()                       |  pass |       |
|                 String getFilename()                 |  pass |       |
|                       get(id)                        |  pass |       |
|                createAndInsert(task)                 |  pass |       |
|                 findOrCreateId(task)                 |  pass |       |
|                update(breakpointInfo)                |  pass |       |
|                     remove (id)                      |  pass |       |
|               getResponseFilename(url)               |  pass |       |
|                 isOnlyMemoryCache()                  |  pass |       |
|                       getId()                        |  pass |       |
|                 setChunked(chunked)                  |  pass |       |
|                     isChunked()                      |  pass |       |
|                 addBlock(blockInfo)                  |  pass |       |
|               isLastBlock(blockIndex)                |  pass |       |
|                   isSingleBlock()                    |  pass |       |
|                 getBlock(blockIndex)                 |  pass |       |
|                     resetInfo()                      |  pass |       |
|                   getBlockCount()                    |  pass |       |
|                  resetBlockInfos()                   |  pass |       |
|                    setEtag(etag)                     |  pass |       |
|                      getEtag()                       |  pass |       |
|                   getTotalOffset()                   |  pass |       |
|                   getTotalLength()                   |  pass |       |
|                copy(): BreakpointInfo                |  pass |       |
|           isSameTaskPendingOrRunning(task)           |  pass |       |
|                   isComplete(task)                   |  pass |       |
|              isCompletedOrUnknown(task)              |  pass |       |
|      startOnSerial (listener: DownloadListener)      |  pass |       |
|     startOnParallel (listener: DownloadListener)     |  pass |       |
| start(listener: DownloadListener, isSerial: Boolean) |  pass |       |
|                      getTasks()                      |pass   |        |
|                    getListener()                     |pass   |        |
|                 getBreakpointStore()                 |pass   |        |
|               getCallbackDispatcher()                |pass   |        |
|                getConnectionFactory()                |pass   |        |
|               getDownloadDispatcher()                |pass   |        |
|                getDownloadStrategy()                 |pass   |        |
|                     getMonitor()                     |pass   |        |
|                       addTag()                       |pass   |        |
|          getInstantBytesPerSecondAndFlush()          |pass   |        |
|             getBytesPerSecondAndFlush()              |pass   |        |
|             getBytesPerSecondFromBegin()             |pass   |        |
|                    instantSpeed()                    |pass   |        |
|                       speed()                        |pass   |        |
|                     lastSpeed()                      |pass   |        |
|           getInstantSpeedDurationMillis()            |pass   |        |
|             getSpeedWithBinaryAndFlush()             |pass   |        |
|               getSpeedWithSIAndFlush()               |pass   |        |
|                    averageSpeed()                    |pass   |        |
|                   speedFromBegin()                   |pass   |        |
|                   getPriority()                      |pass   |        |
|                 getIsWifiRequired()                  |pass   |        |
|                       getTag()                       |pass   |        |
|                   getParentPath()                    |pass   |        |






