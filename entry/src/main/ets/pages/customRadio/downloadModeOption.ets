/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class DownloadMode {
  name: string;
  image: Resource;

  constructor(name:string, image:Resource) {
    this.name = name;
    this.image = image;
  }
}

@Component
export default struct DownloadModeOption {
  @State private downloadModes: DownloadMode[] = [
    new DownloadMode("Serial", $r("app.media.radio_checked")),
    new DownloadMode("Parallel", $r("app.media.radio_unchecked"))
  ];
  @State private downloadModesIndices: number[] = this.downloadModes.map((item, index) => index);
  @Link isSerial: number;
  @Prop isEnable: boolean;

  build() {
    Flex({ justifyContent: FlexAlign.SpaceAround }) {
      ForEach(this.downloadModesIndices, (index:number) => {
        Row({space: 15}) {
          Image(this.isSerial == index ? $r("app.media.radio_checked") : $r("app.media.radio_unchecked"))
            .width('30vp')
            .height('30vp')
            .objectFit(ImageFit.Contain)
          Text(this.downloadModes[index].name)
            .fontSize('20fp')
        }.onClick(() => {

          this.isSerial = index;
        })
      }, (index:string) => index)
    }.width('100%').enabled(this.isEnable).opacity(this.isEnable ? 1 : 0.5)
  }
}