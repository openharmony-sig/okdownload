/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {BreakpointInfo} from '../../breakpoint/BreakpointInfo'
import {ListenerAssist} from './ListenerAssist'
import {DownloadTask} from '../../DownloadTask'

export class ListenerModelHandler <T extends ListenerModel> implements ListenerAssist {
  singleTaskModel: T;
  modelList: Map<number, T> = new Map;
  private alwaysRecoverModel: boolean;
  private creator: ModelCreator<T>;

  constructor(creator: ModelCreator<T>) {
    this.creator = creator;
  }

  public isAlwaysRecoverAssistModel(): boolean {
    return this.alwaysRecoverModel != null && this.alwaysRecoverModel;
  }

  public setAlwaysRecoverAssistModel(isAlwaysRecoverModel: boolean): void {
    this.alwaysRecoverModel = isAlwaysRecoverModel;
  }

  public setAlwaysRecoverAssistModelIfNotSet(isAlwaysRecoverAssistModel: boolean): void {
    if (this.alwaysRecoverModel == null) this.alwaysRecoverModel = isAlwaysRecoverAssistModel;
  }

  addAndGetModel(task: DownloadTask, info: BreakpointInfo): T {
    let model: T = this.creator.create(task.getId());
    if (this.singleTaskModel == null) {
      this.singleTaskModel = model;
    } else {
      this.modelList.set(task.getId(), model);
    }

    if (info != null) {
      model.onInfoValid(info);
    }

    return model;
  }

  getOrRecoverModel(task: DownloadTask, info: BreakpointInfo) {
    const id: number = task.getId();

    let model: T = null;
    if (this.singleTaskModel != null && this.singleTaskModel.getId() === id) model = this.singleTaskModel;
    if (model == null) model = this.modelList.get(id);

    if (model != null || !this.isAlwaysRecoverAssistModel()) return model;

    model = this.addAndGetModel(task, info);
    return model;
  }

  removeOrCreate(task: DownloadTask, info: BreakpointInfo): T {
    const id: number = task.getId();
    let model: T = null;
    if (this.singleTaskModel != null && this.singleTaskModel.getId() === id) {
      model = this.singleTaskModel;
      this.singleTaskModel = null;
    } else {
      model = this.modelList.get(id);
      this.modelList.delete(id);
    }

    if (model == null) {
      model = this.creator.create(id);

      if (info != null) {
        model.onInfoValid(info);
      }
    }

    return model;
  }
}

export interface ModelCreator<T extends ListenerModel> {
  create(id: number): T;
}

export interface ListenerModel {
  getId(): number;

  onInfoValid(info: BreakpointInfo): void;
}